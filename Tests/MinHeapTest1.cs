﻿using System;
using NUnit.Framework;

namespace Astar.Tests
{
	[TestFixture]
	public class MinHeapTest1
	{
		[Test]
		public void TestMethod()
		{
			var m = new VVSuprun.Collections.Generic.MinHeap<int>();
			m.Insert(9);
			m.Insert(4);
			m.Insert(5);
			m.Insert(6);
			Assert.AreEqual(4, m.GetMin());
			Assert.AreEqual(4, m.ExtractMin());
			Assert.AreEqual(5, m.ExtractMin());
			Assert.AreEqual(6, m.ExtractMin());
			Assert.AreEqual(9, m.ExtractMin());
			m.Insert(7);
			m.Insert(3);
			m.Insert(100);
			m.Insert(1);
			Assert.AreEqual(1, m.ExtractMin());
			m.Insert(0);
			Assert.AreEqual(0, m.ExtractMin());
			Assert.AreEqual(3, m.ExtractMin());
			Assert.AreEqual(7, m.ExtractMin());
			m.Insert(50);
			Assert.AreEqual(50, m.ExtractMin());
			Assert.AreEqual(100, m.ExtractMin());
		}
	}
}

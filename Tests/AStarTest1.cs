﻿using System;
using NUnit.Framework;

using VVSuprun.PathFinding;
using VVSuprun.PathFinding.Generic;

namespace Astar.Tests
{
	[TestFixture]
	public class AStarTest1
	{
		[Test]
		public void TestMethod()
		{
            /*
                 1x8.       3456
                 2x7.  <=>  9012
                 3xx6       5678
                 .45.       1234      
            */

            var n1 = new Node3d(new Vector3d(0, 0, 0));
            var n2 = new Node3d(new Vector3d(1, 0, 0));
            var n3 = new Node3d(new Vector3d(2, 0, 0));
            var n4 = new Node3d(new Vector3d(3, 0, 0));

            var n5 = new Node3d(new Vector3d(0, 1, 0));
            var n6 = new Node3d(new Vector3d(1, 1, 0));
            var n7 = new Node3d(new Vector3d(2, 1, 0));
            var n8 = new Node3d(new Vector3d(3, 1, 0));

            var n9 = new Node3d(new Vector3d(0, 2, 0));
            var n10 = new Node3d(new Vector3d(1, 2, 0));
            var n11 = new Node3d(new Vector3d(2, 2, 0));
            var n12 = new Node3d(new Vector3d(3, 2, 0));

            var n13 = new Node3d(new Vector3d(0, 3, 0));
            var n14 = new Node3d(new Vector3d(1, 3, 0));
            var n15 = new Node3d(new Vector3d(2, 3, 0));
            var n16 = new Node3d(new Vector3d(3, 3, 0));

            n9.LinkWith(n13);
            n9.LinkWith(n5);
            n5.LinkWith(n1);
            n5.LinkWith(n2);
            n1.LinkWith(n2);
            n2.LinkWith(n3);
            n3.LinkWith(n8);
            n3.LinkWith(n4);
            n4.LinkWith(n8);
            n8.LinkWith(n11);
            n8.LinkWith(n12);
            n11.LinkWith(n12);
            n11.LinkWith(n15);
            n11.LinkWith(n16);
            n12.LinkWith(n16);
            n12.LinkWith(n15);
            n16.LinkWith(n15);          

            var astar = new AStar<Node3d>();
            var path = astar.FindPath(n13, n15);

            Assert.IsNotNull(path);
            Assert.AreEqual(8, path.Length);
            Assert.AreEqual(0, path[2].position.x);
            Assert.AreEqual(1, path[5].position.y);
		}
	}
}

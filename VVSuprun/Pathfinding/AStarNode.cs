﻿using System;
using System.Collections.Generic;

namespace VVSuprun.PathFinding
{
    public abstract class AStarNode : IComparable
    {
        public double fScore;
        public double gScore;        
        public abstract List<AStarNode> Neighbors { get; }        
        public abstract void AddNeighbor(AStarNode n);
        public abstract void LinkWith(AStarNode n);
        public abstract double DistanceTo(AStarNode other);
        public abstract int CompareTo(object obj);
        public override abstract int GetHashCode();
        public override abstract bool Equals(object obj);        
    }
}
﻿using System;
using System.Collections.Generic;

namespace VVSuprun.PathFinding
{
    public class Node3d : AStarNode
    {
        private static int freeIndex = 0;

        public readonly Vector3d position;
        private List<AStarNode> neighbors = new List<AStarNode>();
        private readonly int index;

        public override double DistanceTo(AStarNode other)
        {
            var o = other as Node3d;
            return Math.Sqrt(Math.Pow(o.position.x - position.x, 2) +
                             Math.Pow(o.position.y - position.y, 2) +
                             Math.Pow(o.position.z - position.z, 2));
        }

        public override List<AStarNode> Neighbors
        {
            get
            {
                return neighbors;
            }
        }

        public override void AddNeighbor(AStarNode n)
        {
            neighbors.Add(n);
        }

        public override void LinkWith(AStarNode n)
        {
            neighbors.Add(n);
            n.AddNeighbor(this);
        }

        public Node3d() : this(new Vector3d(0, 0, 0)) { }

        public Node3d(Vector3d position)
        {
            index = freeIndex++;
            this.position = position;
        }

        public override int CompareTo(object obj)
        {
            if (obj == null) throw new NullReferenceException();
            var n = obj as Node3d;
            if ((object)n == null) throw new ArgumentException("Object is not a Node3d.");            
            if (n.fScore > fScore) return -1;
            else if (n.fScore == fScore) return 0;
            else return 1;
        }

        public override int GetHashCode()
        {
            return index;
        }

        public override bool Equals(object obj)
        {
            return (CompareTo(obj) == 0);
        }
    }
}
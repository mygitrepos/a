﻿using System;
using System.Collections.Generic;
using VVSuprun.PathFinding;
using VVSuprun.Collections.Generic;

namespace VVSuprun.PathFinding.Generic
{
    public class AStar<TNode> where TNode : AStarNode
    {
        /// <summary>
        /// Returns null if unreachable.
        /// </summary>
        public TNode[] FindPath(TNode start, TNode goal, out HashSet<TNode> visited)
        {
            TNode tmp = start; // to avoid later reverse of path array
            start = goal;
            goal = tmp;

            var closed = new HashSet<TNode>();
            var open = new HashSet<TNode>();
            var cameFrom = new Dictionary<TNode, TNode>();
            var best = new MinHeap<TNode>();

            open.Add(start);
            start.gScore = 0;
            start.fScore = start.DistanceTo(goal);
            best.Insert(start);

            while (open.Count > 0)
            {
                TNode current = best.ExtractMin();

                while (!open.Contains(current)) current = best.ExtractMin();

                if (current == goal)
                {
                    visited = closed;
                    return ReconstructPath(cameFrom, current);
                }

                closed.Add(current);
                open.Remove(current);

                foreach (TNode neighbor in current.Neighbors)
                {
                    var newG = current.gScore + current.DistanceTo(neighbor);

                    if (!closed.Contains(neighbor) || newG < neighbor.gScore)
                    {
                        var openContainsNeighbor = open.Contains(neighbor);

                        if (!openContainsNeighbor || neighbor.gScore > newG)
                        {
                            neighbor.gScore = newG;
                            neighbor.fScore = newG + neighbor.DistanceTo(goal);
                            best.Insert(neighbor);
                            cameFrom[neighbor] = current;
                            if (!openContainsNeighbor) open.Add(neighbor);
                        }
                    }
                }
            }

            visited = closed;
            return null;
        }

        private TNode[] ReconstructPath(Dictionary<TNode, TNode> cameFrom, TNode current)
        {
            var path = new List<TNode>();
            path.Add(current);

            while (cameFrom.ContainsKey(current))
            {
                current = cameFrom[current];
                path.Add(current);
            }

            return path.ToArray();
        }

        /// <summary>
        /// Returns null if unreachable.
        /// </summary>
        public TNode[] FindPath(TNode start, TNode goal)
        {
            var visited = new HashSet<TNode>();
            return FindPath(start, goal, out visited);
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace VVSuprun.Collections.Generic
{
    public class MinHeap<T> where T : IComparable
    {
        private List<T> list;
        private const int defaultCapacity = 1024;
        private const int rootIndex = 1;

        public MinHeap() : this(defaultCapacity) { }

        public MinHeap(int capacity)
        {
            list = new List<T>(capacity);
            list.Add(default(T));
        }

        public void Insert(T item)
        {
            list.Add(item);
            int itemIndex = list.Count - 1;
            int parentIndex = itemIndex >> 1;

            while (itemIndex > rootIndex && list[parentIndex].CompareTo(list[itemIndex]) > 0)
            {
                T temp = list[parentIndex];
                list[parentIndex] = list[itemIndex];
                list[itemIndex] = temp;
                itemIndex = parentIndex;
                parentIndex = itemIndex >> 1;
            }
        }

        public T GetMin()
        {
            return list[rootIndex];
        }

        private void MinHeapify(int i)
        {
            int left = 2 * i;
            int right = left + 1;
            int smallest = i;
            int lastIndex = list.Count - 1;

            if (left <= lastIndex && list[left].CompareTo(list[smallest]) < 0)
            {
                smallest = left;
            }

            if (right <= lastIndex && list[right].CompareTo(list[smallest]) < 0)
            {
                smallest = right;
            }

            if (smallest != i)
            {
                T temp = list[smallest];
                list[smallest] = list[i];
                list[i] = temp;
                MinHeapify(smallest);
            }
        }

        public T ExtractMin()
        {
            T min = list[rootIndex];
            list[rootIndex] = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);
            MinHeapify(rootIndex);
            return min;
        }
    }
}